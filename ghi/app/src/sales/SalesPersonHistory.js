import React from 'react';



class SalesPersonHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales: [],
            sales_person: '',
            salespeople: [],
        };
        this.handleChangeSalesperson = this.handleChangeSalesperson.bind(this);
        this.handleChangeSalesRecord = this.handleChangeSalesRecord.bind(this);

    }


    async handleChangeSalesRecord(event) {
        const value = event.target.value;
        event.preventDefault();
        this.setState({ sales_person: value })
        //get a list of sales for specific sales person, using their employee id = value
        const url = `http://localhost:8090/api/salesrecords/salesperson/history/${value}/`;
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ sales: data.sales })
        }
    }
    async componentDidMount() {
        //list all sales people endpoint
        const url = 'http://localhost:8090/api/salesperson/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ salespeople: data.sales_person })

        }
    }

    handleChangeSalesperson(event) {

        this.setState({ sales_person: event.target.name })
    }
    render() {
        return (
            <>
                <div className="px-4 py-5 my-5 mt-0 text-center">
                    <img className="bg-white rounded shadow d-block mx-auto mb-4" alt="" width="600" />
                    <h2 className="display-5 fw-bold mb-5 mt-5">Personnel Sales History</h2>

                </div>
                <div className="mb-3 my-5">
                    <select onChange={this.handleChangeSalesRecord} value={this.state.sales_person} required id="sales_person" name="sales_person" className="form-select">
                        <option value="">Choose a sales person</option>
                        {this.state.salespeople.map(sales_person => {
                            return (
                                <option name={sales_person.name} key={sales_person.id} value={sales_person.id}>{sales_person.name} </option>
                            );
                        })}
                    </select>
                </div>

                <table className="table">
                    <thead>
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.sales.length === 0 && this.state.sales_person !== '' ? <tr><td>no sales history</td></tr> : this.state.sales
                            .map(sale => {
                                return (
                                    <tr key={sale.id}>
                                        <td>{sale.sales_person.name}</td>
                                        <td>{sale.customer.name}</td>
                                        <td>{sale.automobile.vin}</td>
                                        <td>${sale.price}</td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>
            </>
        );
    }
}

export default SalesPersonHistory;


