import React from 'react';
import { useNavigate } from "react-router-dom";

function reFresh(Component) {
    return (props) => <Component {...props} useNavigate={useNavigate()} />;
}


class ManufacturerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
        };
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = { ...this.state }
        const url = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            this.setState({
                name: '',
            });

            this.props.useNavigate("/manufacturers")
        }

    }
    render() {
        return (
            <div className="row">
                <div className="offset-4 col-8">
                    <div className="shadow p-3 mt-4">
                        <h2 className="mb-5 mt-5">Create Manufacturer</h2>
                        <form onSubmit={this.handleSubmit} id="create-manufacturer-form">
                            <div className="form-floating mb-4">
                                <input onChange={this.handleChangeName} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="manufacturer">Name</label>

                            </div>
                            <button className="btn btn-primary" id="addAutoBtn">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default reFresh(ManufacturerForm);
