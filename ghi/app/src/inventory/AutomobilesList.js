import React from 'react';

class AutomobilesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            automobiles: [],
        };
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/automobiles/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const requests = [];
                for (let automobile of data.autos) {
                    const autoUrl = `http://localhost:8100${automobile.href}`;
                    requests.push(fetch(autoUrl));
                }
                const responses = await Promise.all(requests);
                const automobiles = [];
                for (const AutomobileResponse of responses) {
                    if (AutomobileResponse.ok) {
                        const details = await AutomobileResponse.json();
                        automobiles.push(details);
                    } else {
                        console.error(AutomobileResponse);
                    }
                }
                this.setState({ automobiles: automobiles });
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <div className="container">
                <h2 className="mb-5 mt-5">Automobiles Inventory</h2>
                <table className="table table-striped align-middle mt-5">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Color</th>
                            <th>Year</th>
                            <th>Manufacturer</th>
                            <th>Model</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.automobiles.map((auto) => {
                            return (
                                <tr key={auto.href}>
                                    <td>{auto.vin}</td>
                                    <td>{auto.color}</td>
                                    <td>{auto.year}</td>
                                    <td>{auto.model.manufacturer.name}</td>
                                    <td>{auto.model.name}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }

}

export default AutomobilesList;
