import React from 'react';

class ServiceTechnicianList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            technicians: [],
        };
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/technicians/';
        try {
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                this.setState({ technicians: data.technician });
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <div className="container">
                <h2 className="mb-5 mt-5">Service Technicians</h2>
                <table className="table table-striped align-middle mt-5">
                    <thead>
                        <tr>
                            <th>Technician Name</th>
                            <th>Employee Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.technicians.map((technician) => {
                            return (
                                <tr key={technician.employee_number}>
                                    <td>{technician.technician_name}</td>
                                    <td>{technician.employee_number}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }

}

export default ServiceTechnicianList;
