import React from "react";
import { useNavigate } from "react-router-dom"

function withExtras(Component) {
    return (props) => (
        <Component {...props} useNavigate={useNavigate()} />
    );
}

class AddServiceTechnicianForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            technician_name: "",
            employee_number: "",
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeTechnicianName = this.handleChangeTechnicianName.bind(this);
        this.handleChangeEmployeeNumber = this.handleChangeEmployeeNumber.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        const techniciansUrl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(techniciansUrl, fetchConfig);
        if (response.ok) {
            this.setState({
                technician_name: "",
                employee_number: "",
            });
            this.props.useNavigate("/technicians/new")
        }
    }

    handleChangeTechnicianName(event) {
        const value = event.target.value;
        this.setState({ technician_name: value });
    }

    handleChangeEmployeeNumber(event) {
        const value = event.target.value;
        this.setState({ employee_number: value });
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-5">
                        <h2 className="mb-5">Create a new service technician</h2>
                        <form onSubmit={this.handleSubmit} id="create-service-technician-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeTechnicianName} value={this.state.technician_name} placeholder="Technician name" required type="text" name="technician-name" id="technician-name" className="form-control" />
                                <label htmlFor="name">Technician name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeEmployeeNumber} value={this.state.employee_number} placeholder="Employee number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                                <label htmlFor="employee_number">Employee number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default withExtras(AddServiceTechnicianForm);
