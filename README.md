# CarCar

The premiere solution for automobile dealership management!

## Team

- Jared - Service microservice
- Bette - Sales microservice

## How to run the project

1.  Install Docker if you do not already have it installed ([Official Docker website](https://www.docker.com/))
2.  Clone this repository to your local machine
3.  CD into the project directory and run the following commands from your terminal:

```
docker volume create beta-data
docker-compose build
docker-compose up
```

NOTE: When you run `docker-compose up` and if you're on macOS, you will see a warning about an environment variable named `OS` being missing. **You can safely ignore this.**

4. You can start adding data via the front-end or via an API client such as [Insomnia](https://insomnia.rest/) using the API information provided below.
5. You will be able to access the project front-end at http://localhost:3000/

## Design diagram

![](./docs/carcar-design-diagram.png)

## Insomnia import

Included is a JSON file that you can import into Insomnia that contains all of the REST API settings for inventory, service, and sales.  To import into Insomnia:

1. Open preferences
2. Go to the Data tab
3. Click on Import Data
4. Select From File
5. Select [this file](./docs/Insomnia_2022-12-12.json) and click on Import

## Inventory

### Manufacturer model

| Fields | Type      | Attributes     |
| ------ | --------- | -------------- |
| name   | CharField | max_length=100 |

### Vehicle model

| Fields       | Type                      | Attributes                                      |
| ------------ | ------------------------- | ----------------------------------------------- |
| name         | CharField                 | max_length=100                                  |
| picture_url  | URLField                  |                                                 |
| manufacturer | ForeignKey (Manufacturer) | related_name="models", on_delete=models.CASCADE |

### Automobile model

| Fields | Type                      | Attributes                                           |
| ------ | ------------------------- | ---------------------------------------------------- |
| color  | CharField                 | max_length=50                                        |
| year   | PositiveSmallIntegerField |                                                      |
| vin    | CharField                 | max_length=17, unique=True                           |
| model  | ForeignKey (Vehicle)      | related_name="automobiles", on_delete=models.CASCADE |

### REST API

| Action                          | Method | URL                                            |
| ------------------------------- | ------ | ---------------------------------------------- |
| List manufacturers              | GET    | `http://localhost:8100/api/manufacturers/`     |
| Create a manufacturer           | POST   | `http://localhost:8100/api/manufacturers/`     |
| Get a specific manufacturer     | GET    | `http://localhost:8100/api/manufacturers/:id/` |
| Update a specific manufacturer  | PUT    | `http://localhost:8100/api/manufacturers/:id/` |
| Delete a specific manufacturer  | DELETE | `http://localhost:8100/api/manufacturers/:id/` |
| List vehicle models             | GET    | `http://localhost:8100/api/models/`            |
| Create a vehicle model          | POST   | `http://localhost:8100/api/models/`            |
| Get a specific vehicle model    | GET    | `http://localhost:8100/api/models/:id/`        |
| Update a specific vehicle model | PUT    | `http://localhost:8100/api/models/:id/`        |
| Delete a specific vehicle model | DELETE | `http://localhost:8100/api/models/:id/`        |
| List automobiles                | GET    | `http://localhost:8100/api/automobiles/`       |
| Create an automobile            | POST   | `http://localhost:8100/api/automobiles/`       |
| Get a specific automobile       | GET    | `http://localhost:8100/api/automobiles/:vin/`  |
| Update a specific automobile    | PUT    | `http://localhost:8100/api/automobiles/:vin/`  |
| Delete a specific automobile    | DELETE | `http://localhost:8100/api/automobiles/:vin/`  |

## Service

The Service microservice integrates with the Inventory API to track service appointments, including indicating "VIP" if the service appointment is for a car purchased from the dealership (based on VIN). This microservice makes an immutable copy of all vins in the form of SalesVinVO, by polling the Inventory API for added vins on a regular basis (currently every 60 seconds).

### SalesVinVO model (value object)

| Fields    | Type                      | Attributes                       |
| --------- | ------------------------- | -------------------------------- |
| vinVO     | CharField                 | max_length=17, unique=True       |
| import_id | PositiveSmallIntegerField | default=0, null=True, blank=True |

### Service technician model

| Fields          | Type      | Attributes                |
| --------------- | --------- | ------------------------- |
| technician_name | CharField | max_length=200            |
| employee_number | CharField | max_length=8, unique=True |

### Service appointment model

| Fields               | Type                            | Attributes                                           |
| -------------------- | ------------------------------- | ---------------------------------------------------- |
| vin                  | CharField                       | max_length=17, null=True, blank=True                 |
| customer_name        | CharField                       | max_length=200                                       |
| vip                  | BooleanField                    | default=False, blank=True, null=True                 |
| appointment_datetime | DateTimeField                   | null=True, blank=True                                |
| technician           | ForeignKey (Service technician) | related_name="appointment", on_delete=models.PROTECT |
| service_reason       | TextField                       |                                                      |
| appointment_status   | CharField                       | max_length=10, choices=statuses, default="Submitted"                                                     |

### REST API

| Action                        | Method | URL                                           |
| ----------------------------- | ------ | --------------------------------------------- |
| List technicians              | GET    | `http://localhost:8080/api/technicians/`      |
| Create a technician           | POST   | `http://localhost:8080/api/technicians/`      |
| View a specific technician    | GET    | `http://localhost:8080/api/technicians/:id/`  |
| Update a specific technician  | PUT    | `http://localhost:8080/api/technicians/:id/`  |
| List appointments             | GET    | `http://localhost:8080/api/appointments/`     |
| Create an appointment         | POST   | `http://localhost:8080/api/appointments/`     |
| Get a specific appointment    | GET    | `http://localhost:8080/api/appointments/:id/` |
| Update a specific appointment | PUT    | `http://localhost:8080/api/appointments/:id/` |


## Sales

Sales Models
- AutomobileVO
- SalesPerson
- PotentialCustomer
- SalesRecord

SalesRecord model has the 3 other models as ForeignKeys to create a salesrecord and to look up a sales person sale's history. We protect all 3 of the foreign keys so we can preserve the salesrecord. In addition, we have unique=True for the salesrecord automobile foreignKey to avoid duplicate automobiles being sold. 

For the PotentialCustomer model, we set a constraints to avoid duplicate customers. 

AutomobileVO is the value object, where we pull data (automobile vin) from inventory microservice through poller. 

api_unsold_automobile_vos shows the unsold automobiles (not in sales records).

### AutomobileVO model (value object)

| Fields      | Type      | Attributes                  |
| ----------- | --------- | --------------------------- |
| vin         | CharField | max_length=17, unique=True  |
| import_href | CharField | max_length=200, unique=True |

### Sales person model

| Fields          | Type                 | Attributes     |
| --------------- | -------------------- | -------------- |
| name            | CharField            | max_length=200 |
| employee_number | PositiveIntegerField | unique=True    |

### Potential customer model

| Fields       | Type      | Attributes     |
| ------------ | --------- | -------------- |
| name         | CharField | max_length=200 |
| address      | CharField | max_length=200 |
| phone_number | CharField | max_length=50  |

### Sales record model

| Fields       | Type                            | Attributes                                                         |
| ------------ | ------------------------------- | ------------------------------------------------------------------ |
| price        | PositiveIntegerField            |                                                                    |
| automobile   | ForeignKey (AutomobileVO)       | related_name="sales_record", unique=True, on_delete=models.PROTECT |
| sales_person | ForeignKey  (SalesPerson)       | related_name="sales_record", on_delete=models.PROTECT              |
| Customer     | ForeignKey (Potential Customer) | related_name="sales_record", on_delete=models.PROTECT              |

### REST API

| Action                               | Method | URL                                            |
| ------------------------------------ | ------ | ---------------------------------------------- |
| List sales people                    | GET    | `http://localhost:8090/api/salesperson/`       |
| Create sales person                  | POST   | `http://localhost:8090/api/salesperson/`       |
| View a specific sales person         | GET    | `http://localhost:8090/api/salesperson/:id/`   |
| Update a specific sales person       | PUT    | `http://localhost:8090/api/salesperson/:id/`   |
| List potential customers             | GET    | `http://localhost:8090/api/salescustomer/`     |
| Create potential customer            | POST   | `http://localhost:8090/api/salescustomer/`     |
| View a specific potential customer   | GET    | `http://localhost:8090/api/salescustomer/:id/` |
| Update a specific potential customer | PUT    | `http://localhost:8090/api/salescustomer/:id/` |
| List sales records                   | GET    | `http://localhost:8090/api/salesrecords/`      |
| Create sales record                  | POST   | `http://localhost:8090/api/salesrecords/`      |
| View a specific sales record         | GET    | `http://localhost:8090/api/salesrecords/:id/`  |
| Update a specific sales record       | PUT    | `http://localhost:8090/api/salesrecords/:id/`  |
| View unsold Cars                     | GET    | `http://localhost:8090/api/automobilevos/unsold` |





